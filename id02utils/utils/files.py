# -*- coding: utf-8 -*-


import os


def filename_with_suffix(frompath, suffix):
    outpathbase = os.path.splitext(frompath)[0]
    outpathext = os.path.splitext(frompath)[1]
    outpath = f"{outpathbase}_{suffix}{outpathext}"
    return outpath

