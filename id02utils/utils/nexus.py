# -*- coding: utf-8 -*-

import re


def followlink(file, ds):
    return file[file[ds].ref].name

def to_str(k):
    if isinstance(k, str):
        return k
    elif isinstance(k, bytes):
        return k.decode()
    else:
        return str(k)

def checkClass(grp, e, klass):
    
    gg = grp
    if e is not None:
        gg = grp[e]
    
    return 'NX_class' in gg.attrs and to_str(gg.attrs['NX_class']) == klass

def classIter(grp, klass):
    """
    List the available entries corresponding to klass
    """
    
    for e in grp:
        if checkClass(grp, e, klass):
            yield e

def getClass(grp, klass):
    """
    List the available entries corresponding to klass
    """
    r = []
    for e in classIter(grp, klass):
        r += [ grp[e], ]
            
    return r
    
def getDetectors(file, entry):
    """
    Get the detector group of a file
    """
    
    E = file[entry]
    
    for i in getClass(E, 'NXinstrument'):
        for d in getClass(i, 'NXdetector'):
            return d
            
    return None

def getPyFAI(file, entry):
    """
    Get the detector group of a file
    """
    
    E = file[entry]
    
    for i in getClass(E, 'NXprocess'):
#        print(i.name)
        for d in getClass(i, 'NXcollection'):
#            print(d.name)
            if d.name.endswith('parameters'):
                return d
            
    return None

def getEntry(file, entry=None):
    entries = [ e for e in classIter(file['/'], 'NXentry') ]
            
    if entry is None:
        if len(entries) > 1:
            raise ValueError("Multiple entries available on file. Must select one.")
        else:
            entry = entries[0]
    elif entry not in entries:
        raise ValueError("Entry {entry} do not exists.")
            
    return entry

def getDefaultData(file, entry=None):
    
    entry = getEntry(file, entry)
    return _getDefaultData(file[entry])        
        
def _getDefaultData(grp):
    
    
    if checkClass(grp, None,'NXdata'):
        r = {}
        
        if 'signal' in grp.attrs:
            r['data'] = grp[to_str(grp.attrs['signal'])]
            
        if 'axes' in grp.attrs:
            r['axes'] = { 'axes': grp.attrs['axes']}
            for a in grp.attrs['axes']:
                dsnam = to_str(a)
                if dsnam != '.':
                    if dsnam in grp:
                        r['axes'][dsnam] = grp[dsnam]
            
        return r
    
    elif 'default' in grp.attrs:
        newpath = to_str(grp.attrs['default'])
        return _getDefaultData(grp[newpath])
    else:
        raise ValueError(f"Cannot find default data")
    

def get_headers(file, entry, pattern = None):
    """
    Get the file headers
    """
    
    detgrp = getDetectors(file, entry)
    
    if detgrp is not None:
        
        r = {}
        
        if 'header' in detgrp:
            for k in detgrp['header']:
                if pattern is None or re.search(pattern, k):
                    r[k] = detgrp['header'][k][()].decode()
        return r, detgrp['header']
                    
    else:
        pyfaiparams = getPyFAI(file, entry)
        
        print(file, entry, pattern, pyfaiparams)
        
        r = {}
        for k in pyfaiparams:
            if pattern is None or re.search(pattern, k):
                r[k] = to_str(pyfaiparams[k][()])
                
        return r, pyfaiparams
        
        
    return r, None

