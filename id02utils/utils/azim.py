# -*- coding: utf-8 -*-

import numpy as np

def azim(data, cx, cy, mask=None):
    Y,X = np.indices(data.shape)
    X = X - cx
    Y = Y - cy
    
#        cmap = mpl.cm.rainbow
#        norm = mpl.colors.LogNorm(vmin=1, vmax=max(data.ravel()))

    #plt.pcolormesh(X,Y,data,cmap=cmap, norm=norm)
    
    d = np.float32(np.sqrt(X**2+Y**2))    
    msk = data >= 0
    
    if mask is not None:
        np.logical_and(msk, mask, out=msk)
    
    saxs = data[msk]
    dh = d[msk]
    
    dhi = np.array(dh, dtype=np.int32)
    
    ring_brightness = np.bincount(dhi, weights=saxs)
    rings = np.bincount(dhi)
    r = np.arange(np.amax(dhi)+1)
    
#    ring_brightness, radius = np.histogram(qh, weights=saxs, bins=qmax)
#    rings,r = np.histogram(qh, bins=qmax) 
#    r = radius[:-1]
    
    with np.errstate(divide='ignore',invalid='ignore'):
        I = ring_brightness/rings
    
    return r, I


def r2q(r, lambdaw, pix_size, distance, **kwargs):
    
    q = 4*np.pi/lambdaw*np.sin(np.arctan(pix_size*r/distance)/2.0)
    return q

def SolidAngleCorrection(r, distance, pix_size, I, **kwargs):
    pixdist = distance**2 + (r*pix_size)**2
    dOmega = distance*pix_size**2/pixdist**1.5
    
    print(np.mean(dOmega), np.std(dOmega))
    
    return I/dOmega
