# -*- coding: utf-8 -*-

import fabio
import os


def getmask_edf(filename):
    
    if not os.path.isfile(filename):
        return None, None
    
    with fabio.open(filename) as f:
        data = f.data
        
        return data >= 0, data



