# -*- coding: utf-8 -*-


import h5py, hdf5plugin
from pprint import pprint
import re

from ..utils.nexus import classIter, getDetectors, getEntry, get_headers

def showList(args):
    
    for f in args.files[0]:
        
        print(f'\n***************************\n{f}\n***************************\n')
    
        with h5py.File(f, 'r') as fd:
            entry = getEntry(fd, args.entry)            
            H, _ = get_headers(fd, entry, args.pattern)
            
            pprint(H)
        

def setValue(args):
    for f in args.files[0]:
        with h5py.File(f, 'a') as fd:
            entry = getEntry(fd, args.entry)
            
            H, Hgrp = get_headers(fd, entry)
            
            if args.key in Hgrp:
                del Hgrp[args.key]
                
            Hgrp[args.key] = args.value


def main():
    import argparse
    
    parser = argparse.ArgumentParser(description="Read/Write headers of ID02 files")
    parser.add_argument('--entry', default=None, help="Select entry. Mandatory if file contains multiple entries.")
    
    sparser = parser.add_subparsers(required=True, title='subcommands', description='valid subcommands')
    
    listparser = sparser.add_parser('list', help="List headers in file")
    listparser.add_argument('-P', dest='pattern', action='store', default=".*", help="REGEX to match")
    listparser.add_argument('files', action='append', nargs='+', help="Input files")
    listparser.set_defaults(func=showList)
    
    setparser = sparser.add_parser('set', help="Set an header in file")
    setparser.add_argument('key', help="Header key", type=str)
    setparser.add_argument('value', help="header value", type=str)
    setparser.add_argument('files', action='append', nargs='+', help="Files to be updated")
    setparser.set_defaults(func=setValue)
    
    
    
    args = parser.parse_args()
    args.func(args)
    
    
    
