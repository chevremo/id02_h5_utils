# -*- coding: utf-8 -*-

import h5py, hdf5plugin
from pprint import pprint
import re
import numpy as np
import scipy as sc
import os, shutil

from ..utils.nexus import followlink, classIter, getEntry, get_headers, getDefaultData
from ..utils.files import filename_with_suffix
from ..utils.mask import getmask_edf
from ..utils.azim import azim, r2q
from .header import get_headers

import matplotlib.pyplot as plt

def sumfiles(args):
    
    frompath = args.files[0][0]
    outpath = filename_with_suffix(frompath, "sum")
    
    shutil.copy(frompath, outpath)
   
    summed_img = None
   
    for f in args.files[0]:
        with h5py.File(f, 'r') as fd:
            entry = getEntry(fd, args.entry)
            
            data = np.array(fd[f"/{entry}/measurement/data"])
            data = np.sum(data, axis=0)
            
            if summed_img is None:
                summed_img = data
            else:
                summed_img += data
                
    summed_img.shape = (1, *summed_img.shape)
                
    with h5py.File(outpath, 'a') as fd:
        entry = getEntry(fd, args.entry)
         
#        del fd[f"/{entry}/measurement/data"]
        ds = followlink(fd, f"/{entry}/measurement/data")
        
        del fd[ds]
        fd[ds] = summed_img
        
def avgfiles(args):
    
    frompath = args.files[0][0]
    outpath = filename_with_suffix(frompath, "averaged")
    
    shutil.copy(frompath, outpath)
   
    img_stack = None
   
    for f in args.files[0]:
        with h5py.File(f, 'r') as fd:
            entry = getEntry(fd, args.entry)
            
            D = getDefaultData(fd, entry)
            
            data = np.array(D['data'])
            
            if len(data.shape) == 2:
                data.shape = (1, *data.shape)
            
            if img_stack is None:
                img_stack = data
            else:
                img_stack = np.concatenate((img_stack, data), axis=0)
                
    #print(img_stack.shape)
    summed_img = np.nanmean(img_stack, axis=0, keepdims=True)
                
    with h5py.File(outpath, 'a') as fd:
        entry = getEntry(fd, args.entry)
         
#        del fd[f"/{entry}/measurement/data"]
        ds = followlink(fd, getDefaultData(fd, entry)['data'].name)
        
        #print(ds)
        
        del fd[ds]
        fd[ds] = summed_img
        
        
def cavefiles(args):
    
    for f in args.files[0]:
        with h5py.File(f, 'r') as fd:
            entry = getEntry(fd, args.entry)
            outpath = filename_with_suffix(f, "cave")
            
            centers, _ = get_headers(fd, entry, "Center_[1-2]")
            flatfield_h, _ = get_headers(fd, entry, "FlatfieldFile(Name|Path)")
            mask_h, _ = get_headers(fd, entry, "MaskFile(Name|Path)")
            
            cx = float(centers['Center_1'])
            cy = float(centers['Center_2'])
            flatfield_filename = os.path.join(flatfield_h['FlatfieldFilePath'], flatfield_h['FlatfieldFileName'])
            mask_filename = os.path.join(mask_h['MaskFilePath'], mask_h['MaskFileName'])
            
            D = getDefaultData(fd, entry)
            data = np.array(D['data'], dtype=np.float64)
            mask, _ = getmask_edf(mask_filename)
            flatm, flat = getmask_edf(flatfield_filename)
            
            #data[mask]
            
#            data /= flat
            
            # Get final mask
            if mask is not None and flatm is not None:
                msk = np.logical_and(mask, flatm)
            elif flatm is not None:
                msk = flatm
            elif mask is not None:
                msk = mask
            else:
                raise ValueError("No mask for caving.")
                
            data[:,~msk] = -10
                
            # Flipping
            data_f = np.flip(data, (1,2))
            msk_f  = np.flip(msk, (0,1))
            
            # Displace
            delta_x = int(2*cx - data.shape[2] - 1)
            delta_y = int(2*cy - data.shape[1] + 1)
            
            data_f = np.roll(data_f, (delta_x, delta_y), axis=(2,1))
            msk_f = np.roll(msk_f, (delta_x, delta_y), axis=(1,0))
            
            # mask rolled values
            if delta_x > 0:
                msk_f[:,:delta_x] = False
            else:
                msk_f[:,delta_x:] = False
                
            if delta_y > 0:
                msk_f[:delta_y, :] = False
            else:
                msk_f[delta_y:, :] = False
            
            # cave
            msk_c = np.logical_and(msk_f, ~msk)
            data_caved = data
            data_caved[:,msk_c] = data_f[:,msk_c]
            
            
            # Save file
            
            shutil.copy(f, outpath)
            
            with h5py.File(outpath, 'a') as fdo:
                 
                ds = followlink(fdo, getDefaultData(fd, entry)['data'].name)
                
                del fdo[ds]
                fdo[ds] = data_caved
                
def combinefiles(args):
    """
    Combine the files, taking care of the center.
    """
    
    if len(args.files[0]) != 2:
        raise ValueError("Need exactly two files.")
        
    f1 = args.files[0][0]
    
    with h5py.File(f1, 'r') as fd:
        entry = getEntry(fd, args.entry)
        outpath = filename_with_suffix(f1, "combined")
        
        centers, _ = get_headers(fd, entry, "Center_[1-2]")
        
        cx1 = float(centers['Center_1'])
        cy1 = float(centers['Center_2'])
        
        data1 = np.array(fd[f"/{entry}/measurement/data"], dtype=np.float64)
        mask1 = data1[0] >= 0
    
    f2 = args.files[0][1]
    
    with h5py.File(f2, 'r') as fd:
        entry = getEntry(fd, args.entry)
        
        centers, _ = get_headers(fd, entry, "Center_[1-2]")
        
        cx2 = float(centers['Center_1'])
        cy2 = float(centers['Center_2'])
        
        data2 = np.array(fd[f"/{entry}/measurement/data"], dtype=np.float64)
        mask2 = data2[0] >= 0
        
    # Displace
    delta_x = int(cx1 - cx2)
    delta_y = int(cy1 - cy2)
    
    data2 = np.roll(data2, (delta_x, delta_y), axis=(2,1))
    mask2 = np.roll(mask2, (delta_x, delta_y), axis=(1,0))
    
    # mask rolled values
    if delta_x > 0:
        mask2[:,:delta_x] = False
    else:
        mask2[:,delta_x:] = False
        
    if delta_y > 0:
        mask2[:delta_y, :] = False
    else:
        mask2[delta_y:, :] = False
        
    # find factor
    r1, I1 = azim(data1[0], cx1, cy1)
    r2, I2 = azim(data2[0], cx1, cy1)
    f = np.mean(I1[480:490]/I2[480:490]) # Peak of PEO, not subject to orientation
        
    data_c = data1.copy()
    msk_c = np.logical_and(mask2, ~mask1)
    
    data_c[:,msk_c] = data2[:,msk_c]*f
    
    # Save file
            
    shutil.copy(f1, outpath)
    
    with h5py.File(outpath, 'a') as fdo:
         
        ds = followlink(fdo, f"/{entry}/measurement/data")
        
        del fdo[ds]
        fdo[ds] = data_c
        
#    plt.plot(r1, I1, r2, I2*f)
#    plt.xscale('log')
#    plt.yscale('log')
#    
#    plt.figure()
#    plt.imshow(np.log(data_c[0]))
#    
#    plt.show()
        
        
def file2spline(args):
    
    f1 = args.file
    with h5py.File(f1, 'r') as fd:
        entry = getEntry(fd, args.entry)
        
        centers, _ = get_headers(fd, entry, "Center_[1-2]")
        
        cx1 = float(centers['Center_1'])
        cy1 = float(centers['Center_2'])
        
        data1 = np.array(fd[f"/{entry}/measurement/data"], dtype=np.float64)
        mask1 = data1[0] >= 0
    
    
    Y, X = np.indices(data1.shape[1:])
    
    X = X - cx1
    Y = Y - cy1
    
    R = (X**2+Y**2)**0.5
    TH= np.arctan2(Y, X)
    
#    plt.imshow(r)
#    plt.figure()
#    plt.imshow(th)
#    plt.show()
    
    N, M = 100,72
    
    rsp = np.logspace(0,np.log10(np.max(R[:])), N+1)
    thsp= np.linspace(-np.pi, np.pi,M+1)
        
    r = (rsp[1:]+rsp[:-1])/2
    th= (thsp[1:]+thsp[:-1])/2
    
    A = np.ones((N,M), dtype=np.float64)*-10
    
    for i in range(N):
        for j in range(M):
        
            mskr = np.logical_and(R >= rsp[i], R < rsp[i+1])
            mskth = np.logical_and(TH >= thsp[j], TH < thsp[j+1])
            msk = np.logical_and(np.logical_and(mskr, mskth), mask1)
            
            if np.any(msk):
                A[i,j] = np.mean(data1[0,msk])
                
#    plt.pcolormesh(rsp, thsp, np.log10(A.T))
    plt.imshow(np.log10(A.T))
    plt.show()
    
    
def cave45(args):
      
    for f in args.files[0]:
        with h5py.File(f, 'r') as fd:
            entry = getEntry(fd, args.entry)
            outpath = filename_with_suffix(f, "cave45")
            
            centers, _ = get_headers(fd, entry, "Center_[1-2]")
            flatfield_h, _ = get_headers(fd, entry, "FlatfieldFile(Name|Path)")
            mask_h, _ = get_headers(fd, entry, "MaskFile(Name|Path)")
            
            cx = float(centers['Center_1'])
            cy = float(centers['Center_2'])
            
            data = np.array(fd[f"/{entry}/measurement/data"], dtype=np.float64)
            mask = data[0] >= 0
            
        # Flipping
        data_r = data.copy()
        Y, X = np.indices(data.shape[1:])
    
        X = X - cx
        Y = Y - cy
        
        R = (X**2+Y**2)**0.5
        TH= np.arctan2(Y, X)
        
        for x, y, r, th in zip(X[~mask], Y[~mask], R[~mask], TH[~mask]):
            
            # Let' s find the closer pixel at 45deg from missing pixel position
            xx = int(cx + r*np.cos(th+np.pi/4))
            yy = int(cy + r*np.sin(th+np.pi/4))
            
#            print(x,y,r,th,xx,yy)
            
            if xx > 0 and xx < data_r.shape[2] and yy > 0 and yy < data_r.shape[1]:
                data_r[:,int(y+cy),int(x+cx)] = data[:,yy,xx]
                
        mask = data_r[0] >= 0
                
        for x, y, r, th in zip(X[~mask], Y[~mask], R[~mask], TH[~mask]):
            
            # Let' s find the closer pixel at 22.5deg from missing pixel position
            xx = int(cx + r*np.cos(th+np.pi/8))
            yy = int(cy + r*np.sin(th+np.pi/8))
            
#            print(x,y,r,th,xx,yy)
            
            if xx > 0 and xx < data_r.shape[2] and yy > 0 and yy < data_r.shape[1]:
                data_r[:,int(y+cy),int(x+cx)] = data[:,yy,xx] 
        
            # Save file
            
        shutil.copy(f, outpath)
        
        with h5py.File(outpath, 'a') as fdo:
             
            ds = followlink(fdo, f"/{entry}/measurement/data")
            
            del fdo[ds]
            fdo[ds] = data_r
            
  
def powlaw(args):
    for f in args.files[0]:
        with h5py.File(f, 'r') as fd:
            entry = getEntry(fd, args.entry)
            outpath = filename_with_suffix(f, "powlaw")
            
            centers, _ = get_headers(fd, entry, "(Center_[1-2]|SampleDistance|WaveLength|PSize_1)")
            
            cx = float(centers['Center_1'])
            cy = float(centers['Center_2'])
            sdd = float(centers['SampleDistance'])
            lambdaw = float(centers['WaveLength'])
            pix_s = float(centers['PSize_1'])
            
            data = np.array(fd[f"/{entry}/measurement/data"], dtype=np.float64)
            
            data_caved = data.copy()
            
            r, I = azim(data[0], cx, cy)
            q = r2q(r, lambdaw, pix_s, sdd)*1e-9
            
            L0 = 30
            L1 = 20
            L2 = 80
            
            kk = np.polyfit(np.log10(q[L1:L2]), np.log10(I[L1:L2]),1)
            
#            plt.plot(q,I)
#            plt.plot(q, 10**np.polyval(kk, np.log10(q)))
#            plt.xscale('log')
#            plt.yscale('log')
            
            Y, X = np.indices(data.shape[1:])
    
            X = X - cx
            Y = Y - cy
            
            R = (X**2+Y**2)**0.5
            Q = r2q(R, lambdaw, pix_s, sdd)*1e-9
            msk = R < L0
            
            cd = 10**np.polyval(kk, np.log10(Q[msk]))
            
            data_caved[:,msk] = cd            
            data_caved[:,int(cy),int(cx)] = np.nanmax(cd)
            
#            rc, Ic = azim(data_caved[0], cx, cy)
#            plt.plot(q,Ic)
#            plt.show()
            
            # Save file
            
            shutil.copy(f, outpath)
            
            with h5py.File(outpath, 'a') as fdo:
                 
                ds = followlink(fdo, f"/{entry}/measurement/data")
                
                del fdo[ds]
                fdo[ds] = data_caved
        
def main():
    import argparse
    
    parser = argparse.ArgumentParser(description="Read/Write headers of ID02 files")
    parser.add_argument('--entry', default=None, help="Select entry. Mandatory if file contains multiple entries.")
    
    sparser = parser.add_subparsers(required=True, title='subcommands', description='valid subcommands')
    
    listparser = sparser.add_parser('sum', help="Sum all images")
    listparser.add_argument('files', action='append', nargs='+', help="Input files")
    listparser.set_defaults(func=sumfiles)
    
    listparser = sparser.add_parser('avg', help="Avg all images")
    listparser.add_argument('files', action='append', nargs='+', help="Input files")
    listparser.set_defaults(func=avgfiles)
    
    caveparser = sparser.add_parser('cave', help="Cave all images")
    caveparser.add_argument('files', action='append', nargs='+', help="Input files")
    caveparser.set_defaults(func=cavefiles)
    
    caveparser = sparser.add_parser('cave45', help="Cave by 45o rotation all images")
    caveparser.add_argument('files', action='append', nargs='+', help="Input files")
    caveparser.set_defaults(func=cave45)
    
    caveparser = sparser.add_parser('powlaw', help="Assume a power law at beam center")
    caveparser.add_argument('files', action='append', nargs='+', help="Input files")
    caveparser.set_defaults(func=powlaw)
    
    caveparser = sparser.add_parser('combine', help="Combine 2 images, taking care of relative intensity")
    caveparser.add_argument('files', action='append', nargs='+', help="Input files")
    caveparser.set_defaults(func=combinefiles)
    
    caveparser = sparser.add_parser('spline', help="Create a 2d spline describing the intensity")
    caveparser.add_argument('file', help="Input file")
    caveparser.set_defaults(func=file2spline)
    
    
    
    args = parser.parse_args()
    args.func(args)

