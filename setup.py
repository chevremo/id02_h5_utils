# -*- coding: utf-8 -*-

import sys
if sys.version_info < (3,6):
    sys.exit('Sorry, Python < 3.6 is not supported')


from setuptools import setup, find_packages

setup(name='id02utils',
      version='0.1',
      python_requires='>3.6',
      description='ESRF ID02 utils for ID02 h5 files manipulation',
      author='William Chevremont',
      author_email='william.chevremont@esrf.fr',
      install_requires=[
          'numpy',
          'scipy',
          'h5py>=3.4',
          'hdf5plugin',
          'argparse',
          'fabio',
          'matplotlib',
      ],
      packages=find_packages(include=['id02utils', 'id02utils.*']),
      entry_points={
                  'console_scripts': ['id02headers=id02utils.bin.header:main',
                                      'id02operation=id02utils.bin.operation:main',
                                    ]
              },
      package_data={},
      include_package_data=False,
    )
